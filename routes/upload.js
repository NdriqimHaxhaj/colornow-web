const express = require('express');
const router = express.Router();
const bodyParser = require("body-parser");
const passport = require('passport');
const session = require('express-session')
const cookieParser = require('cookie-parser');
const http = require('http');

router.use(bodyParser.urlencoded({
    extended: true
}));

router.use(session({
    secret: 'oigmfhofigh',
    resave: false,
    saveUninitialized: true
    // cookie: { secure: true }
}))
router.use(passport.initialize());
router.use(passport.session());
router.use(cookieParser());
router.get('/', function(req, res, next) {

    console.log("user_id: "+req.user);
    if (req.isAuthenticated()) {
        res.render('upload', { title: 'Login | Color Now App' })
    } else {
        res.redirect('/login');
        res.end();
    }
});


// Upload file on /svg_files
router.post('/', function(req, res) {

    if (!req.files) {
        console.log('No files attached');
        res.redirect('/home')
    }

    // The name of the input field (i.e. "svgFile") is used to retrieve the uploaded file
    const svgFile = req.files.svgFile;

    // Generate unique name based on timestamp
    var timestampName = new Date().getTime().toString();

    // Add extension
    const filename = timestampName + '.svg'
    console.log(filename)

    // Use the mv() method to place the file somewhere on your server
    const fullPath = './svg_files/'+filename
    console.log("Full Path: " + fullPath)

    svgFile.mv(fullPath, function(err) {
        console.log("executing svgFile.mv");
        if (err) {
            console.log('Error uploading file !');
            res.redirect('/home')
        }
        var connection = require('../db');
        var query = 'INSERT INTO images (`id`, `image_name`) VALUES ('+ req.user+', "'+ filename+'")';
        console.log(query);
        connection.query(query, function (error, results, fields) {
            if (error) {
                console.log("Error putting details into images table")
            } else {
                console.log('File uploaded successfully');
            }


            const url = 'http://colornowapp.com:3000/extract/'+timestampName;
            console.log(url)
            http.get(url, (resp) => {
                let data = '';

                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    const stringArray = JSON.parse(data);
                    var values = "";
                    for (i=0;i<stringArray.length;i++){
                        values += '("' + filename + '","' +stringArray[i]+ '"),';
                    }
                    values = values.substring(0, values.length - 1);

                    query = 'INSERT INTO `image_paths`(`id`, `path`) VALUES '+ values
                    console.log(query);
                    connection.query(query, function (error, results, fields) {
                        if (error) {
                            console.log(error);
                            console.log("Error putting paths into image_paths");
                        } else {
                            console.log(error);
                        }

                        res.redirect('/home')
                        res.end();
                    });
                });

            }).on("error", (err) => {
                console.log("Error: " + err.message);
            });
        });
    });
});

passport.serializeUser(function(user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function(user_id, done) {
    done(null, user_id);
});

module.exports = router;