const express = require('express');
const router = express.Router();
const bodyParser = require("body-parser");
const passport = require('passport');
const session = require('express-session')
const cookieParser = require('cookie-parser');


router.use(bodyParser.urlencoded({
    extended: true
}));

router.use(session({
    secret: 'oigmfhofigh',
    resave: false,
    saveUninitialized: true
    // cookie: { secure: true }
}))
router.use(passport.initialize());
router.use(passport.session());
router.use(cookieParser());

router.get('/', function(req, res, next) {
    res.render('login', { title: 'Login | Color Now App' });
});

router.post('/', function(req, res) {
    var connection = require('../db');
    var query = 'SELECT * FROM users WHERE username = "'+ req.body.username+'" AND password ="'+ req.body.password+'"';
    connection.query(query, function (error, results, fields) {
        if (error) {
            res.redirect('/login');
            res.end();
        }

        const user_id = results[0]["id"];
        req.login(user_id, function(err){
            res.redirect('/home');
            res.end();
        });

    });
});

passport.serializeUser(function(user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function(user_id, done) {
    done(null, user_id);
});

module.exports = router;