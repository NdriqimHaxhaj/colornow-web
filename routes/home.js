const express = require('express');
const router = express.Router();
const session = require('express-session')
const passport = require('passport');
const cookieParser = require('cookie-parser');


router.use(session({
    secret: 'oigmfhofigh',
    resave: false,
    saveUninitialized: true
    // cookie: { secure: true }
}))
router.use(passport.initialize());
router.use(passport.session());
router.use(cookieParser());

router.get('/', function(req, res, next) {
    console.log("user_id : "+req.user);
    console.log(req.isAuthenticated());
    if (req.isAuthenticated()) {
        var connection = require('../db');
        var query = 'SELECT image_name FROM images WHERE id = "'+ req.user+'"';
        connection.query(query, function (error, results, fields) {
            if (error) {
                console.log("Error getting data")
            }
            var images = [];
            results.forEach(function(element) {
                images.push(element["image_name"])
            });

            res.render('index', { title: 'Home | Color Now App', images: images, items: images.length });
        });
    } else {
        res.redirect('/login');
        res.end();
    }

});

passport.serializeUser(function(user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function(user_id, done) {
    done(null, user_id);
});

module.exports = router;