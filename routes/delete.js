const express = require('express');
const router = express.Router();
const bodyParser = require("body-parser");
const passport = require('passport');
const session = require('express-session')
const cookieParser = require('cookie-parser');
const http = require('http');

router.use(bodyParser.urlencoded({
    extended: true
}));

router.use(session({
    secret: 'oigmfhofigh',
    resave: false,
    saveUninitialized: true
    // cookie: { secure: true }
}))
router.use(passport.initialize());
router.use(passport.session());
router.use(cookieParser());

// Delete file
router.get('/:id', function (req, res, next) {
    const imageName = req.params.id+'.svg';
    console.log(imageName);
    var connection = require('../db');
    const removeImagePathsQuery = `DELETE FROM image_paths WHERE id='`+imageName+`'`;
    connection.query(removeImagePathsQuery, function (error, results, fields) {

        if (error){
            console.log("Error deleting image_paths");
            console.log(error);
            res.redirect('/home');
            res.end();
        } else {
            const removeImageIDQuery = `DELETE FROM images WHERE image_name='`+imageName+`'`;
            connection.query(removeImageIDQuery, function (error,results,fields) {
               if (error){
                   console.log(error);
                   console.log("Error deleting image_id");
                   res.redirect('/home');
                   res.end();
               } else {
                   console.log("Succesfully deleted image");
                   res.redirect('/home');
                   res.end();
               }
            });
        }
        
    });

});

module.exports = router;