// Require express and create an instance of it
const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const parse = require('parse-svg-path')
const extract = require('extract-svg-path')
const fs = require('fs')
const bodyParser = require("body-parser");
const session = require('express-session')
const passport = require('passport')
const cookieParser = require('cookie-parser');
const groupBy = require('group-by')
const Client = require('ftp');

// default options
app.use(fileUpload());
app.use('/static', express.static('public'));
app.use('/images', express.static('public/images'));
app.use('/drawings', express.static('svg_files'));
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(session({
    secret: 'oigmfhofigh',
    resave: false,
    saveUninitialized: true
    // cookie: { secure: true }
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());


var login = require('./routes/login');
var home = require('./routes/home');
var upload = require('./routes/upload');
var deletion = require('./routes/delete');

// Root - colornowapp:3000
app.get('/', function (req, res) {
    if (req.isAuthenticated()){
        res.redirect('/home');
    } else {
        res.redirect('/login');
    }
    res.end();
});

// Log user out
app.get('/logout', function (req, res) {
    req.logout();
    req.session.destroy();
    res.redirect('/login');
    res.end();
});

// Login
app.use('/login', login);

// Home
app.use('/home', home);

// Upload form
app.use('/upload', upload);

// Deletion routes
app.use('/delete', deletion);

// Extract path for a file with specific id
app.get('/extract/:id', function (req, res) {
    const fileURL = __dirname + '/svg_files/'+req.params.id+'.svg'

    if (fs.existsSync(fileURL)) {
        var paths = extract(__dirname + '/svg_files/'+req.params.id+'.svg')
        // console.log(paths);
        var strings = [];
        var string = "";
        for(i=0;i<paths.length;i++){
            if (paths[i]== "z"){
                string += paths[i];
                strings.push(string);
                string = "";
                continue
            }
            string += paths[i];
        }
        console.log(strings);
        res.send(JSON.stringify(strings));
        var strings = [];
        var string = "";
    } else {
        res.send('{"status":'+false+',' +
            ' "paths":'+null+'}');
    }
});

// Extract path for a file with specific id
app.get('/getalldrawings', function (req, res) {
    var connection = require('./db');
    // var query = `SELECT id, GROUP_CONCAT(CONCAT('{path: "', path, ' "}')) as patharr FROM image_paths GROUP BY id`;

    var query = `SELECT * FROM image_paths`;
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) {
            console.log("Error putting details into images table")
        } else {
            res.send(groupBy(results, 'id'));
            res.end();
            console.log('File uploaded successfully');
        }


    });
});


passport.serializeUser(function(user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function(user_id, done) {
    done(null, user_id);
});

// Change the 404 message modifing the middleware
app.use(function(req, res, next) {
    res.status(404).send("Sorry, that route doesn't exist. Have a nice day :)");
});

// start the server in the port 3000 !
app.listen(3000, function () {
    console.log('Server listening on port 3000.');
});